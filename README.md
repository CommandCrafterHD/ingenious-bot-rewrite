# Ingenious Bot rewrite

![Version](https://img.shields.io/badge/Version-1.1.0-2222ff.svg)

A mostly python project to create a discord Bot with a nice Webinterface
and Some cool in-chat functions/games!

### The commands
|**Command**|**Description**|**For everyone**|**Mods only**|**Admin only**|**Dev Only**|
|-----------|---------------|----------------|-------------|--------------|------------|
|help|Displays all commands that are available to you/gives more info like syntaxes for commands!|:white_check_mark:|
|about|Shows you neat stuff about the bot!|:white_check_mark:|
|future|Shows you what we are working on and what is planned for the future!|:white_check_mark:|
|me|Shows you neat stuff about yourself!|:white_check_mark:|
|permission_list|Shows you what permission level corresponds to which "role"|:white_check_mark:|
|trivia|Asks you a random question!|:white_check_mark:|
|blackjack|Lets you play blackjack in Discord!|:white_check_mark:|
|pie_chart|Shows you a nice pie-chart with your values!|:white_check_mark:|
|poll|Lets you start a poll of your own|:white_check_mark:|
|leetify|Lets you create hacker-like texts!|:white_check_mark:|
|uwufy|Lets you create "cute" texts!|:white_check_mark:|
|coin_flip|Flips a coin for you!|:white_check_mark:|
|password|Generates a password with a default length of 16!|:white_check_mark:|
|dice|Rolls a six sided die!|:white_check_mark:|
|yahtzee|Lets you play 42/18 in chat!|:white_check_mark:|
|bottlespin|Picks one random person who's in the same voice channel as you!|:white_check_mark:|
|large_emoji|Gives you the large version of (almost) any emoji!|:white_check_mark:|
|todo|Lets you add/remove/check off items of your ToDo list|:white_check_mark:|
|create_embed|Lets you create simple embeds!||:white_check_mark:|
|welcome_message|Sets/disables the server-wide welcome message!|||:white_check_mark:|
|private_welcome_message|Sets/disables the private welcome message!|||:white_check_mark:|
|voice_log|Enables/disables logging of all voice-channel activity!|||:white_check_mark:|
|permission_set|Lets you change someones permissions!|||:white_check_mark:|
|reload_module|Reloads certain modules! (The entire cogs)||||:white_check_mark:|
|eval|Lets you run python Code from within Discord chat||||:white_check_mark:|
|halt|Halts all bot functionallity!||||:white_check_mark:|
|resume|Resumes all bot functionallity!||||:white_check_mark:|
|restart|Restarts the bot (Technically it just stops it :P)!||||:white_check_mark:|
|shell|Lets you run commands in the directory the bot is currently running in!||||:white_check_mark:|

## Made by
@CommandCrafterHD

@Romangraef