# Importing stuffs, as always...
from files import common
from utility import get_user
from discord.ext import commands
from discord.ext.commands import Context as CommandContext, CommandError


class PermissionLevel(CommandError):
    pass


class BotOnHalt(CommandError):
    pass


def permission_check(needed: int):
    def predicate(ctx: CommandContext):
        pl = get_user(uid=ctx.author.id, gid=ctx.guild.id)

        if str(ctx.author.id) in common.config['admins']:
            return True
        elif pl.permission_level >= needed:
            return True
        elif ctx.author.guild_permissions.administrator and needed < 4:
            return True
        else:
            raise PermissionLevel("You do not have the permissions to use this command!")

    return commands.check(predicate)

