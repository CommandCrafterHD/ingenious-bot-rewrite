import os

path = os.path.realpath(__file__)
name_length = len(os.path.basename(__file__))
os.chdir(path[:-name_length])

# Importing some important stuffs
from utility import get_server
from files import colors, common
from checks import BotOnHalt
import yaml
from discord import Message
from discord.ext import commands

# Opening the settings file and writing said settings to the common.py file
try:
    file = open("files/config.yaml", "r")
    config = yaml.load(file.read(), Loader=yaml.SafeLoader)
    for i in config.keys():
        common.config[i] = config[i]
    common.config["halted"] = False
except FileNotFoundError:
    # Creating a new file if it doesnt exist
    print(f"{colors.RED}ERROR: File {colors.YELLOW}/files/config.yaml{colors.RED} not found! Creating it!"
          f"\nPlease make sure you edit it with all the needed values!{colors.END}")
    new_file_content = "#This is the config file of the Ingenious-Bot!\n" \
                       "#You will have to put all important settings into this file!\n" \
                       "---\n" \
                       "#Your ID, the user with this ID has the permission_level -1 pre-set!\n" \
                       "\"admin\": \"YOUR ID HERE\"\n" \
                       "#This is the token of your bot (I hope you know how to get it, otherwise giyf)\n" \
                       "\"token\": \"YOUR TOKEN HERE\"\n" \
                       "#This is the bots standard-prefix (pretty self-explanatory)\n" \
                       "\"prefix\": \"!\"\n" \
                       "\n" \
                       "# Apex Stuff get them here https://www.apexlegendsapi.com\n" \
                       "\"api_key\": \"\"\n" \
                       "\n" \
                       "# Web-Server\n" \
                       "# The client ID of your bot (I hope you know how to get it, otherwise giyf)\n" \
                       "\"client_id\": \"\"\n" \
                       "# The client secret of your bot (I hope you know how to get it, otherwise giyf)\n" \
                       "\"client_secret\": \"\"\n" \
                       "# The session secret key (This can be randomly generated, it doesn't really\n" \
                       "# matter als long as you don't give it away to anyone!\n" \
                       "\"session_secret_key\": \"\"\n"
    try:
        new_file = open("files/config.yaml", "w+")
        new_file.write(new_file_content)
        new_file.close()
        quit(404)
    except PermissionError:
        # Telling the user when the file couldn't be created due to missing permissions
        print(f"{colors.RED}Could not create file! Does the bot have sufficient rights? (Quiting)")
        quit(500)


# Getting the server-specific prefix
def get_server_prefix(bot: commands.Bot, message: Message):
    if not message.guild:
        return common.config["prefix"]
    prefix = get_server(message.guild.id).prefix
    return commands.when_mentioned_or(prefix)(bot, message)


# Setting up our bot-client
client: commands.bot = commands.AutoShardedBot(command_prefix=get_server_prefix)


@client.before_invoke
async def before_any_command(ctx: commands.Context):
    if common.config["halted"]:
        if ctx.command.name.lower() in ["resume", "halt"]:
            pass
        else:
            raise BotOnHalt("The bot is currently on halt!")
    else:
        pass
    try:
        await ctx.message.add_reaction("✅")
    except:
        pass


# Listing all module-files in the /modules folder
MODULES = []
for i in os.listdir("modules"):
    if i.endswith('.py'):
        MODULES.append('modules.' + i[:-3])
    else:
        continue

# Removing the integrated (useless) help command
client.remove_command('help')

# Importing the previously listed commands
print("--"*20)
print(f"{colors.YELLOW}LOADING MODULES!{colors.END}")
print("--"*20)
for module in MODULES:
    try:
        client.load_extension(module)
        print(f"{colors.CYAN}Module {module}{colors.END}: {colors.GREEN}LOADED{colors.END}")
    except Exception as e:
        print(f"{colors.CYAN}Module {module}{colors.END}: {colors.RED}NOT LOADED\n({e}){colors.END}")
print("--"*20)

# Running the bot
if __name__ == "__main__":
    client.run(common.config["token"])
