from discord.ext import commands
from discord.ext.commands import Context as CommandContext, Cog
from utility import syntax
from discord import Embed, Colour
import random
import asyncio

# ┏━━━┓
# ┃ X ┃
# ┃ X ┃
# ┗━━━┛
# ♠ ♦ ♣ ♥

example_deck = []


def fill_example():
    global example_deck
    example_deck = [
            dict(
                number=rank,
                symbol=color)
            for rank in [CardParts.KING, CardParts.QUEEN, CardParts.JOKER, CardParts.ACE, *range(2, 11)]
            for color in [CardParts.HEARTS, CardParts.SPADES, CardParts.DIAMONDS, CardParts.CLUBS]]


class CardParts:
    TOP = "┏━━━┓"
    MIDDLE = "┃{}┃"
    BOTTOM = "┗━━━┛"
    CLUBS = "♣"
    HEARTS = "♥"
    DIAMONDS = "♦"
    SPADES = "♠"
    QUEEN = "Q"
    KING = "K"
    JOKER = "J"
    ACE = "A"


def render_cards(cards: list):
    result = ""
    worth = 0
    for card in cards:
        if card["number"] in [CardParts.KING, CardParts.QUEEN, CardParts.JOKER, CardParts.ACE]:
            if card["number"] == CardParts.ACE:
                worth += 1
            else:
                worth += 10
        else:
            worth += int(card["number"])
    result += ''.join([CardParts.TOP for i in cards]) + '\n'
    _numbers = []
    for i in cards:
        if i['number'] is not 10:
            _numbers.append(f" {str(i['number'])} ")
        else:
            _numbers.append(f" {str(i['number'])}")
    result += ''.join([CardParts.MIDDLE.format(_numbers[i]) for i in range(0, len(cards))]) + '\n'
    result += ''.join([CardParts.MIDDLE.format(" " + i['symbol'] + " ") for i in cards]) + '\n'
    result += ''.join([CardParts.BOTTOM for i in cards])
    return [result, worth]


class BlackJackCog(Cog):
    def __init__(self, bot: commands.Bot):
        self.bot: commands.Bot = bot

    @commands.command(description="It's blackjack... in Discord!")
    @syntax('', 'explanation')
    async def blackjack(self, ctx: CommandContext, sub_command: str = None):
        if sub_command is not None and sub_command.lower() == "explanation":
            await ctx.send(embed=Embed(
                title="Blackjack",
                description="So basically you take cards until you say stop. Every card has a certain worth:\n"
                            "\n"
                            "**A**ce = 1\n"
                            "**2-10** = Their respective numbers\n"
                            "**J**oker / **K**ing / **Q**ueen = 10\n"
                            "\n"
                            "Your goal is to stop at 21. If you get over, you loose."
            ))
            return
        elif sub_command is not None:
            await ctx.send(embed=Embed(
                title="Error",
                description=f"Subcommand **{sub_command}** not found! Try **{ctx.prefix}help blackjack** for more info!",
                color=Colour.red()
            ))
            return
        cards = example_deck
        used = []
        card = random.choice(cards)
        used.append(card)
        cards.remove(card)
        rendered = render_cards(used)
        em = Embed(
            title="Your cards:",
            description=f"```\n{rendered[0]}\n```\nYou are currently at **{rendered[1]}** points!\n\n:o: - To play another card\n:x: - To stop\n",
            color=Colour.blurple()
        )
        em.set_footer(text="You have 30 seconds to react to this message!")
        message = await ctx.message.channel.send(embed=em)
        await message.add_reaction("⭕")
        await message.add_reaction("❌")

        def check(reaction, user):
            if reaction.emoji in ["⭕", "❌"] and user.id == ctx.author.id:
                return reaction

        async def another_card():
            try:
                reaction = await self.bot.wait_for('reaction_add', timeout=30.0, check=check)
            except asyncio.TimeoutError:
                await message.edit(embed=Embed(
                    title="Timed out!",
                    description=f"Your time to react ran out, sorry!",
                    color=Colour.red()
                ))
                await message.clear_reactions()
                return
            else:
                if reaction[0].emoji == "⭕":
                    card = random.choice(cards)
                    used.append(card)
                    cards.remove(card)
                    rendered = render_cards(used)
                    if rendered[1] > 21:
                        em = Embed(
                            title="You Lost",
                            description=f"```\n{rendered[0]}\n```\nYou got **{rendered[1]}** points! **{rendered[1]-21}** too many!",
                            color=Colour.red()
                        )
                        await message.edit(embed=em)
                        await message.clear_reactions()
                        return
                    elif rendered[1] == 21:
                        em = Embed(
                            title="You Won!",
                            description=f"```\n{rendered[0]}\n```\nYou got exactly **{rendered[1]}** points!",
                            color=Colour.green()
                        )
                        await message.edit(embed=em)
                        await message.clear_reactions()
                        return
                    else:
                        em = Embed(
                            title="Your cards:",
                            description=f"```\n{rendered[0]}\n```\nYou are currently at **{rendered[1]}** points!\n\n:o: - To play another card\n:x: - To stop\n",
                            color=Colour.blurple()
                        )
                        em.set_footer(text="You have 30 seconds to react to this message!")
                        await message.edit(embed=em)
                        await message.remove_reaction("⭕", ctx.message.author)
                        await another_card()
                elif reaction[0].emoji == "❌":
                    rendered = render_cards(used)
                    em = Embed(
                        title="You stopped:",
                        description=f"```\n{rendered[0]}\n```\nYou got **{rendered[1]}** points!",
                        color=Colour.blurple()
                    )
                    await message.edit(embed=em)
                    await message.clear_reactions()
                    return

        await another_card()


def setup(bot: commands.Bot):
    fill_example()
    bot.add_cog(BlackJackCog(bot))
