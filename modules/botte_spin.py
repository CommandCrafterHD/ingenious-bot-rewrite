from discord import Embed, Colour
from discord.ext import commands
from discord.ext.commands import Context as CommandContext, Cog
from utility import syntax
from asyncio import sleep
from random import choice


class BottleSpinCog(Cog):
    def __init__(self, bot: commands.Bot):
        self.bot: commands.Bot = bot

    @commands.command(description="Gives you a random user from your voice channel, that is not you.")
    @syntax('')
    async def bottlespin(self, ctx: CommandContext):
        if ctx.author.voice:
            members = [x for x in ctx.author.voice.channel.members if not x.bot]
            members.remove(ctx.author)
            if members:
                msg = await ctx.send(embed=Embed(
                    title="Bottle Spin",
                    description="Spinning the bottle!",
                    color=Colour.gold()
                ))
                await sleep(3)
                await msg.edit(embed=Embed(
                    title="Bottle Spin",
                    description=f"The bottle landed on: {choice(members).mention}!",
                    color=Colour.green()
                ))
            else:
                await ctx.send(embed=Embed(
                    title="Error",
                    description="You are alone in your voice channel, playing like this would be pretty boring!",
                    color=Colour.red()
                ))
        else:
            await ctx.send(embed=Embed(
                title="Error",
                description="You are not connected to a voice channel!",
                color=Colour.red()
            ))


def setup(bot: commands.Bot):
    bot.add_cog(BottleSpinCog(bot))
