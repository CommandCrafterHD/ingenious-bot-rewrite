from files import common
from utility import syntax
from discord import Embed, Colour, __version__
from discord.ext import commands
from discord.ext.commands import Context as CommandContext, Cog


class HelpCog(Cog):
    def __init__(self, bot: commands.Bot):
        self.bot: commands.Bot = bot

    @syntax('')
    @commands.command()
    async def about(self, ctx: CommandContext):
        em = Embed(
            title="Ingenious Bot",
            description="",
            color=Colour.blurple()
        )
        em.set_thumbnail(url=ctx.bot.user.avatar_url)
        em.add_field(name="Made by", value="<@137259132305539072> and <@310702108997320705>", inline=False)
        em.add_field(name="General Information", value="Python version: **3.6**\n"
                                                       f"Discord.py version: **{__version__}**\n"
                                                       f"Ping: **{str(ctx.bot.latency)[:4]} Seconds**\n"
                                                       f"Prefix: **{ctx.prefix}**", inline=False)
        em.add_field(name="Need help?", value=f"For all commands, try **{ctx.prefix}help**\n"
                          "If you encounter any bugs or issues, just DM <@137259132305539072> about it!", inline=False)
        em.add_field(name="Important links",
                     value="[GitLab](https://gitlab.com/CommandCrafterHD/ingenious-bot-rewrite)\n"
                           "Webpanel (**COMING SOON**)", inline=False)
        await ctx.send(embed=em)

    @syntax('')
    @commands.command()
    async def future(self, ctx: CommandContext):
        await ctx.send(embed=Embed(
            title="What we're working on/planning for the bot",
            description="```md\n"
                        "### Planned for the future ###\n"
                        "<Apex-Legends-Player-lookup We just need to find a good API to use!>\n"
                        "<Image-manipulation Stuff like creating memes!>\n"
                        "<Voice-Channel-Logs Let everyone know who was in which channel at what time!>\n"
                        "<Custom-Commands So you can finally have your own commands!>\n"
                        "\n"
                        "### Currently WIP ###\n"
                        "<Music-player So you can listen to your favourite tracks while talking!>\n"
                        "<Web-Panel A place to manage the bot from the interwebs!>"
                        "```",
            color=Colour.blurple()
        ))

    @syntax("<command>")
    @commands.command()
    async def help(self, ctx: CommandContext, cmd: str = None):
        txt = f"```md\n### Here is a list of all modules and their corresponding commands! ###\nUse {ctx.prefix}help <command> to get more info about a command!```\n```diff\n"
        cog_list = []
        if not cmd:
            for cog in self.bot.cogs:
                name = cog
                if len(self.bot.get_cog(cog).get_commands()) == 0:
                    continue
                if name.lower().endswith("cog"):
                    name = name[:-3]
                if name == "System":
                    if str(ctx.author.id) not in common.config["admins"]:
                        continue
                    else:
                        name = "System (DEV ONLY)"
                cmds = [name]
                for cmd in self.bot.get_cog(cog).get_commands():
                    cmds.append(ctx.prefix + str(cmd.name))
                cog_list.append(cmds)
            cog_list.sort(key=len)
            for i in cog_list:
                txt += f"\n- {i[0]}\n"
                for a in i[1:]:
                    txt += f"   {a}\n"
            txt += "```"
            await ctx.message.channel.send(txt)
        else:
            if self.bot.get_command(cmd):
                command = self.bot.get_command(cmd)
                cmd_syntax = ""
                if hasattr(command.callback, 'syntax'):
                    for i in command.callback.syntax:
                        cmd_syntax += f"**{ctx.prefix}{cmd.lower()} {i}**\n"
                else:
                    cmd_syntax = f"**{ctx.prefix}{cmd.lower()}**"
                em = Embed(
                    color=Colour.blurple(),
                    title="Help",
                    description="")
                em.add_field(name="Command", value=cmd.title(), inline=False)
                if command.description:
                    em.add_field(name="Description", value=command.description, inline=False)
                em.add_field(name="Syntax", value=cmd_syntax, inline=False)
                await ctx.send(embed=em)
            else:
                await ctx.send(
                    embed=Embed(
                        title="Error",
                        description=f"The command **{cmd.lower()}** cannot be found! Use **{ctx.prefix}help** for a list of commands!",
                        color=Colour.red()))


def setup(bot: commands.Bot):
    bot.add_cog(HelpCog(bot))
