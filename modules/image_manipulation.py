from discord.ext import commands
from discord.ext.commands import Context as CommandContext, Cog
from discord import Emoji, File
from utility import syntax
from discord import Embed, Colour
from PIL import Image
import requests
import os
from typing import Union
from svglib.svglib import svg2rlg
import svgutils
from reportlab.graphics import renderPM


class ImageManipulationCog(Cog):
    def __init__(self, bot: commands.Bot):
        self.bot: commands.Bot = bot

    @commands.command(description="Reverses gifs from sources like gify and tenor!")
    @syntax('<Link to your gif>')
    async def reverse_gif(self, ctx: CommandContext, gif: str):
        name = gif.split('/')[-1]
        if name.endswith('.gif'):
            try:
                with open('files/tmp/' + gif.split('/')[-1], 'wb') as f:
                    f.write(requests.get(gif).content)
                    im = Image.open('files/tmp/' + name)
                    frames = im.get_frames()
                    frames.reverse()
                    frames[0].save('files/tmp/' + name, save_all=True, append_images=frames[1:])
                    await ctx.send(file=File(open('files/tmp/' + name)))
                    os.remove('files/tmp/' + name)
            except Exception as e:
                print(e)
                await ctx.send(embed=Embed(
                    title="Error",
                    description="GIF could not be retrieved/reversed! If this is a valid link, contact the bot authors!",
                    color=Colour.red()
                ))

    @commands.command(description="Gives you the image of an emoji to work with!")
    @syntax('<Emoji>')
    async def large_emoji(self, ctx: CommandContext, emoji: Union[Emoji, str]):
        if isinstance(emoji, Emoji):
            em = Embed(
                title="Done!",
                description="",
                color=Colour.blurple()
            )
            em.set_image(url=emoji.url)
            em.add_field(name="Created at", value=emoji.created_at, inline=False)
            await ctx.message.channel.send(embed=em)
        else:
            if not hex(ord(emoji))[2:] + ".svg" in os.listdir("files/svgs"):
                print("New emoji! Lets download the svg! (" + hex(ord(emoji)) + " " + hex(ord(emoji))[2:] + ")")
                # Download the emoji from twemoji
                r = requests.get("https://twemoji.maxcdn.com/svg/%s.svg" % hex(ord(emoji))[2:])
                # Give an error if it doesn't exist on twemoji (for example if its just a string)
                if r.status_code == 404:
                    await ctx.send(embed=Embed(
                        title="Error",
                        description="Emoji not found! Either this emoji does not exist or it is just not availabe on [Twemoji](https://github.com/twitter/twemoji)!\nFor example :stop_button: and :play_pause: seem to be missing from twemoji!",
                        color=Colour.red()
                    ))
                    return

                # Save the file
                with open('files/svgs/%s.svg' % hex(ord(emoji))[2:], 'wb') as f:
                    f.write(r.content)

                # Open the original with svg2rlg to get the original size
                svg = svg2rlg(open("files/svgs/%s.svg" % hex(ord(emoji))[2:]))
                # Open the original with svgutils to resize it
                originalSVG = svgutils.compose.SVG("files/svgs/%s.svg" % hex(ord(emoji))[2:])
                # Scale it up 3 times
                originalSVG.scale(3)
                # Composit it back to a figure
                figure = svgutils.compose.Figure(float(svg.height) * 3, float(svg.width) * 3, originalSVG)
                # Save said figure
                figure.save("files/svgs/%s.svg" % hex(ord(emoji))[2:])
                # Open the new saved figure with svg2rlg
                file = svg2rlg(open("files/svgs/%s.svg" % hex(ord(emoji))[2:]))
                # Save the file as png
                renderPM.drawToFile(file, "files/pngs/%s.png" % hex(ord(emoji))[2:], fmt='PNG', bg=renderPM.colors.transparent)
                em = Embed(
                    title="Done!",
                    description="This version of the emoji was provided by [Twemoji](https://github.com/twitter/twemoji)!",
                    color=Colour.blurple()
                )
                # Send the png
                await ctx.message.channel.send(file=File(open("files/pngs/%s.png" % hex(ord(emoji))[2:], "rb")), embed=em)


def setup(bot: commands.Bot):
    if "tmp" not in os.listdir("files"):
        os.mkdir("files/tmp")
    if "svgs" not in os.listdir("files"):
        os.mkdir("files/svgs")
    if "pngs" not in os.listdir("files"):
        os.mkdir("files/pngs")
    bot.add_cog(ImageManipulationCog(bot))
