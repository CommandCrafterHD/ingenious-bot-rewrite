from discord import Embed, Colour, TextChannel
from discord.ext import commands
from discord.ext.commands import Context as CommandContext, Cog
from utility import syntax, get_server
from typing import Union
import checks


class LoggingCog(Cog):
    def __init__(self, bot: commands.Bot):
        self.bot: commands.Bot = bot

    @commands.command()
    @syntax('', '<Channel Mention>', 'disable')
    @checks.permission_check(3)
    async def voice_log(self, ctx: CommandContext, channel: Union[TextChannel, str] = None):
        if not channel:
            guild = get_server(ctx.guild.id)
            if guild.voice_log_active:
                await ctx.send(embed=Embed(
                    title="Voice logging",
                    color=Colour.blurple(),
                    description=f"The current channel set up for voice logging is: {ctx.guild.get_channel(guild.voice_log_channel).mention}!"
                ))
            else:
                await ctx.send(embed=Embed(
                    title="Voice logging",
                    color=Colour.blurple(),
                    description=f"Voice logging is currently not enabled! To enable it use:\n**{ctx.prefix}voice_log <channel mention>**"
                ))
            return
        if isinstance(channel, TextChannel):
            guild = get_server(ctx.guild.id)
            guild.voice_log_channel = channel.id
            guild.voice_log_active = True
            guild.save()
            await ctx.send(embed=Embed(
                title="Voice logging",
                color=Colour.green(),
                description=f"Logging channel updated! Voice channel activity will now be logged to <#{channel.id}>"
            ))
        else:
            if channel.lower() == "disable":
                guild = get_server(ctx.guild.id)
                if guild.voice_log_active:
                    guild.voice_log_active = False
                    guild.save()
                    await ctx.send(embed=Embed(
                        title="Voice logging",
                        color=Colour.blurple(),
                        description=f"Voice channel activity will no longer be logged!"
                    ))
                else:
                    await ctx.send(embed=Embed(
                        title="Voice logging",
                        color=Colour.red(),
                        description=f"Voice channel activity logging is already disabled!"
                    ))
            else:
                await ctx.send(embed=Embed(
                    title="Error",
                    color=Colour.red(),
                    description=f"**{channel}** is not a valid argument!\n\nUse **{ctx.prefix}help voice_log** for more info!"
                ))


def setup(bot: commands.Bot):
    bot.add_cog(LoggingCog(bot))
