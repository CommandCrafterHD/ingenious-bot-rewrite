import matplotlib.pyplot as plt
from utility import syntax
from discord import Embed, Colour, File
from discord.ext import commands
from discord.ext.commands import Context as CommandContext, Cog

colors = ['gold', 'yellowgreen', 'lightcoral', 'lightskyblue', 'orange', 'darkseagreen', 'peru', 'mediumvioletred', 'tomato', 'seagreen']


class MathCog(Cog):
    def __init__(self, bot: commands.Bot):
        self.bot: commands.Bot = bot

    @commands.command()
    @syntax('<Slice Name>:<Slice Value> ...')
    async def pie_chart(self, ctx: CommandContext, *, data: str):
        data = data.split(" ")
        if data:
            if len(data) > 10:
                await ctx.send(embed=Embed(
                    color=Colour.red(),
                    title="Error",
                    description="You can't give more then 10 labels!"
                ))
                return
            else:
                labels = []
                used_colors = []
                sizes = []
                for data_pack in data:
                    labels.append(data_pack.split(":")[0].title())
                    used_colors.append(colors[data.index(data_pack)])
                    try:
                        sizes.append(data_pack.split(":")[1])
                    except:
                        await ctx.channel.send(embed=Embed(
                            color=Colour.red(),
                            title="Error",
                            description=f"Could not use label **{data_pack}**!"
                        ))
                        break
                if not len(sizes) == len(data):
                    print(str(sizes) + " - " + str(data))
                    return
                else:
                    plt.pie(sizes, labels=labels, colors=colors, autopct='%1.1f%%', shadow=True, startangle=140)

                    plt.axis('equal')
                    plt.gcf().savefig("files/pie_chart.png", dpi=100)
                    plt.close(plt.gcf())
                    await ctx.channel.send(file=File("files/pie_chart.png"))
        else:
            await ctx.send(embed=Embed(
                color=Colour.red(),
                title="Error",
                description="You need to give at least one label to show!"
            ))


def setup(bot: commands.Bot):
    bot.add_cog(MathCog(bot))
