from files import common
from discord import opus
from discord.ext import commands
from discord.ext.commands import Context as CommandContext, Cog
from discord import Embed, Colour, PCMVolumeTransformer, FFmpegPCMAudio
from utility import syntax
import urllib.request
import urllib.parse
import re
import checks
import youtube_dl
import asyncio

if not opus.is_loaded():
    print("ERROR! Opus could not be loaded!")

youtube_dl.utils.bug_reports_message = lambda: ''

ytdl_format_options = {
    'format': 'bestaudio/best',
    'outtmpl': '%(extractor)s-%(id)s-%(title)s.%(ext)s',
    'restrictfilenames': True,
    'noplaylist': True,
    'nocheckcertificate': True,
    'ignoreerrors': False,
    'logtostderr': False,
    'quiet': True,
    'no_warnings': True,
    'default_search': 'auto',
    'source_address': '0.0.0.0' # bind to ipv4 since ipv6 addresses cause issues sometimes
}

ffmpeg_options = {
    'options': '-vn'
}

ytdl = youtube_dl.YoutubeDL(ytdl_format_options)


class YTDLSource(PCMVolumeTransformer):
    def __init__(self, source, *, data, volume=0.5):
        super().__init__(source, volume)

        self.data = data

        self.title = data.get('title')
        self.url = data.get('url')

    @classmethod
    async def from_url(cls, url, *, loop=None, stream=False):
        loop = loop or asyncio.get_event_loop()
        data = await loop.run_in_executor(None, lambda: ytdl.extract_info(url, download=not stream))

        if 'entries' in data:
            # take first item from a playlist
            data = data['entries'][0]

        filename = data['url'] if stream else ytdl.prepare_filename(data)
        return cls(FFmpegPCMAudio(filename, **ffmpeg_options), data=data)


class MusicCog(Cog):
    def __init__(self, bot: commands.Bot):
        self.bot: commands.Bot = bot

    @checks.permission_check(1)
    @syntax('')
    @commands.command(description="Makes the bot join your channel!")
    async def joinme(self, ctx: CommandContext):
        try:
            connection = common.voice_channels[str(ctx.guild.id)]
        except:
            connection = None
        if not ctx.author.voice:
            await ctx.send(embed=Embed(
                title="Error",
                description="It seems like you are not in a voice channel!",
                color=Colour.red()
            ))
        else:
            if not connection:
                connection = await ctx.author.voice.channel.connect(timeout=30.0, reconnect=True)
                common.voice_channels[str(ctx.guild.id)] = connection
                await ctx.send(embed=Embed(
                    title="Connected",
                    description="Joined the channel " + connection.channel.name,
                    color=Colour.green()
                ))
            else:
                if connection.is_connected():
                    connection = await connection.move_to(ctx.author.voice.channel)
                    common.voice_channels[str(ctx.guild.id)] = connection
                    await ctx.send(embed=Embed(
                        title="Connected",
                        description="Joined the channel " + connection.channel.name,
                        color=Colour.green()
                    ))
                else:
                    connection = await ctx.author.voice.channel.connect(timeout=30.0, reconnect=True)
                    common.voice_channels[str(ctx.guild.id)] = connection
                    await ctx.send(embed=Embed(
                        title="Connected",
                        description="Joined the channel " + connection.channel.name,
                        color=Colour.green()
                    ))

    @checks.permission_check(1)
    @syntax('')
    @commands.command(description="Makes the bot leave your channel!")
    async def leaveme(self, ctx: CommandContext):
        try:
            connection = common.voice_channels[str(ctx.guild.id)]
        except:
            connection = None
        if not ctx.author.voice:
            await ctx.send(embed=Embed(
                title="Error",
                description="It seems like you are not in a voice channel!",
                color=Colour.red()
            ))
        else:
            await connection.disconnect(force=True)
            await ctx.send(embed=Embed(
                title="Disconnected",
                description="Left your channel!",
                color=Colour.green()
            ))

    @checks.permission_check(1)
    @syntax('<Youtube Link>')
    @commands.command(description="Plays songs from youtube!")
    async def play(self, ctx: CommandContext, url: str = None):
        if url:
            try:
                connection = common.voice_channels[str(ctx.guild.id)]
            except:
                connection = None
            if not connection:
                await ctx.send(embed=Embed(
                    title="Error",
                    description="It seems like the bot is not in a voice channel!\nUse **{ctx.prefix}joinme** to make it appear in your channel!",
                    color=Colour.red()
                ))
                return
            else:
                player = await YTDLSource.from_url(url, loop=self.bot.loop, stream=True)
                connection.play(player, after=lambda e: print('Player error: %s' % e) if e else None)
                await ctx.send(embed=Embed(
                    title="Started Playing",
                    description=f"Started playing: **{player.title}**!",
                    color=Colour.green()
                ))

    @checks.permission_check(1)
    @syntax('<Video title>')
    @commands.command(description="Plays songs from youtube!")
    async def search(self, ctx: CommandContext, title: str = None):
        if title:
            try:
                connection = common.voice_channels[str(ctx.guild.id)]
            except:
                connection = None
            if not connection:
                await ctx.send(embed=Embed(
                    title="Error",
                    description="It seems like the bot is not in a voice channel!\nUse **{ctx.prefix}joinme** to make it appear in your channel!",
                    color=Colour.red()
                ))
                return
            else:
                query_string = urllib.parse.urlencode({"search_query": input()})
                html_content = urllib.request.urlopen("http://www.youtube.com/results?" + query_string)
                search_results = re.findall(r'href=\"\/watch\?v=(.{11})', html_content.read().decode())
                player = await YTDLSource.from_url("http://www.youtube.com/watch?v=" + search_results[0], loop=self.bot.loop, stream=True)
                connection.play(player, after=lambda e: print('Player error: %s' % e) if e else None)
                await ctx.send(embed=Embed(
                    title="Started Playing",
                    description=f"Started playing: **{player.title}**!",
                    color=Colour.green()
                ))

    @syntax('<volume from 1 to 10>')
    @checks.permission_check(1)
    @commands.command(description="Lets you change the bots volume!")
    async def volume(self, ctx: CommandContext, new_volume: float = None):
        try:
            connection = common.voice_channels[str(ctx.guild.id)]
        except:
            connection = None
        if not connection:
            await ctx.send(embed=Embed(
                title="Error",
                description="It seems like the bot is not in a voice channel!\nUse **{ctx.prefix}joinme** to make it appear in your channel!",
                color=Colour.red()
            ))
            return
        else:
            if new_volume:
                if 0 < new_volume <= 10:
                    connection.source.volume = new_volume
                    await ctx.send(embed=Embed(
                        title="Current volume",
                        description="The current volume is: " + str(connection.source.volume),
                        color=Colour.blurple()
                    ))
                else:
                    await ctx.send(embed=Embed(
                        title="Error",
                        description="The volume needs to be a number from 1 to 10!",
                        color=Colour.red()
                    ))
            else:
                await ctx.send(embed=Embed(
                    title="Current volume",
                    description="The current volume is: " + str(connection.source.volume),
                    color=Colour.blurple()
                ))


def setup(bot: commands.Bot):
    bot.add_cog(MusicCog(bot))
