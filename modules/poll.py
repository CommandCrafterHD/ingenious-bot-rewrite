from discord.ext import commands
from discord.ext.commands import Context as CommandContext, Cog
from discord import Embed, Color
from utility import syntax


class PollCog(Cog):
    def __init__(self, bot: commands.Bot):
        self.bot: commands.Bot = bot

    @commands.command(description="Lets you create simple polls, just send a question, followed by a question mark or colon(This is optional, you can leave the question away too) after that send all the answers, seperated by spaces. If you want to have longer questions or answers just surround them in quotation marks!")
    @syntax("<question>: <answer> <answer>...", "<question>? <answer> <answer>...", "<answer> <answer>...")
    async def poll(self, ctx: CommandContext, *options: str):

        if len(options) <= 1:
            await ctx.send(embed=Embed(title="Error", colour=Color.red(), description="Needs options to choose from!"))
            return

        if len(options) > 20:
            await ctx.send(embed=Embed(title="Error", colour=Color.red(), description="You can't have more than 20 options."))
            return

        em = Embed(color=Color.blue(), description=ctx.message.author.mention + " has started a poll:")
        em.set_author(name=ctx.message.author.display_name, icon_url=ctx.message.author.avatar_url)

        msg = ""

        if options[0][-1] in [':', '?']:
            if len(options) == 1:
                await ctx.send(embed=Embed(title="Error", colour=Color.red(), description="Needs options to choose from!"))
                return
            field_name = options[0]
            options = options[1:]
        else:
            field_name = "No Question given:"

        for i in range(len(options)):
            msg += "\n" + chr(i + 65) + ": " + options[i]

        em.add_field(name=field_name, value=msg)

        sent_msg = await ctx.message.channel.send(embed=em)

        for i in range(len(options)):
            await sent_msg.add_reaction(chr(0x1F1E6 + i))


def setup(bot: commands.Bot):
    bot.add_cog(PollCog(bot))
