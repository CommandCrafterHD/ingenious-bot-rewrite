import checks
from discord import Embed, Color
from discord.ext import commands
from discord.ext.commands import Context as CommandContext, Cog
from utility import syntax, get_server


class PrefixCog(Cog):
    def __init__(self, bot: commands.Bot):
        self.bot: commands.Bot = bot

    @commands.command()
    @syntax("<Your Prefix>")
    @checks.permission_check(3)
    async def prefix(self, ctx: CommandContext, prefix: str = None):
        if prefix:
            if len(prefix) > 10:
                await ctx.send(embed=Embed(
                    title="Error",
                    description="Why would you create a prefix longer than 10 characters?!",
                    color=Color.red()
                ))
                return
            guild = get_server(ctx.guild.id)
            guild.prefix = prefix
            guild.save()
            await ctx.send(embed=Embed(
                title="New prefix",
                description=f"The new prefix is: ```{prefix}```",
                color=Color.green()
            ))
        else:
            current_prefix = get_server(ctx.guild.id).prefix
            await ctx.send(embed=Embed(
                title="Current prefix",
                description=f"The current prefix is: ```{current_prefix}```",
                color=Color.blurple()
            ))


def setup(bot: commands.Bot):
    bot.add_cog(PrefixCog(bot))
