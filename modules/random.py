import random, asyncio, string
from discord import Embed, Colour, DMChannel
from discord.ext import commands
from discord.ext.commands import Context as CommandContext, Cog
from utility import syntax


class RandomCog(Cog):
    def __init__(self, bot: commands.Bot):
        self.bot: commands.Bot = bot

    @commands.command()
    async def coin_flip(self, ctx: CommandContext):
        msg = await ctx.channel.send(embed=Embed(
            color=Colour.orange(),
            title="Coin Flip",
            description="Flipping..."
        ))
        await asyncio.sleep(2)
        await msg.edit(embed=Embed(
            color=Colour.blurple(),
            title="Coin Flip",
            description=random.choice(['HEADS!', 'TAILS!'])
        ))

    @commands.command()
    async def dice(self, ctx: CommandContext):
        msg = await ctx.channel.send(embed=Embed(
            color=Colour.orange(),
            title="Dice",
            description="Rolling the die..."
        ))
        await asyncio.sleep(2)
        await msg.edit(embed=Embed(
            color=Colour.blurple(),
            title="Dice",
            description=random.choice([":one:", ":two:", ":three:", ":four:", ":five:", ":six:"])
        ))

    @commands.command(description="Use this command to generate random strings of letters, numbers and symbols! You can also specify a length yourself, if you leave it as is it will generate a 16 character strong password!")
    @syntax('', '<Length>')
    async def password(self, ctx: CommandContext, length: int = 16):
        if length > 1000 or length < 1:
            await ctx.send(embed=Embed(
                title="Error",
                color=Colour.red(),
                description="You need to give a length that is over 0 and under 1000! (Who can even remember a 1000 symbol password?!)"
            ))
            return
        choices = string.ascii_letters + string.digits + "#!?."
        text = ''.join([random.choice(choices) for i in range(0, length+1)])
        server_info = ''
        if not isinstance(ctx.message.channel, DMChannel):
            server_info = '(WARNING: You should never use a password made by this bot in a server you dont trust, since everyone can see this message! Message the bot in private if you want to be safe!)\n'
        await ctx.send(embed=Embed(
            title="Password generated!",
            color=Colour.blurple(),
            description=f"{server_info}```fix\n{text}```"
        ))


def setup(bot: commands.Bot):
    bot.add_cog(RandomCog(bot))
