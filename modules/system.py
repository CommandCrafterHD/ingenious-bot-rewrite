import os
import drutils
import checks
import socket
import subprocess
import datetime
from files import common, colors
from time import gmtime, strftime
from utility import syntax, get_server
from checks import PermissionLevel, BotOnHalt
from discord import Embed, Colour as Color, Status
from discord.ext import commands
from discord.ext.commands import Context as CommandContext, Cog
from discord.ext.commands import MissingPermissions, CommandNotFound, MissingRequiredArgument, BadArgument


class SystemCog(Cog):
    def __init__(self, bot: commands.Bot):
        self.bot: commands.Bot = bot

    @Cog.listener()
    async def on_ready(self):
        print(f"{colors.YELLOW}BOT STARTED!{colors.END}")
        print("--"*20)
        print(f"{colors.CYAN}Name{colors.END}: {colors.YELLOW}{self.bot.user.name}{colors.END}")
        print(f"{colors.CYAN}ID{colors.END}: {colors.YELLOW}{self.bot.user.id}{colors.END}")
        print("--" * 20)
        for i in self.bot.guilds:
            get_server(i.id, add_if_none=True)

    @Cog.listener()
    async def on_voice_state_update(self, member, before, after):
        guild = get_server(member.guild.id)
        if guild.voice_log_active:
            time = strftime("%d-%m-%Y %H:%M:%S", gmtime())
            timezone = datetime.datetime.now(datetime.timezone.utc).astimezone().tzinfo
            if not before.channel and after.channel:
                em = Embed(
                    title="Voice Log",
                    color=Color.green(),
                    description=f"Member {member.mention} joined voice channel {after.channel.mention} :inbox_tray:"
                )
                em.set_footer(text=f':clock1: {time} (Timezone: {timezone})')
                await member.guild.get_channel(channel_id=guild.voice_log_channel).send(embed=em)
            elif before.channel and not after.channel:
                em = Embed(
                    title="Voice Log",
                    color=Color.red(),
                    description=f"Member {member.mention} left voice channel {before.channel.mention} :outbox_tray:"
                )
                em.set_footer(text=f':clock1: {time} (Timezone: {timezone})')
                await member.guild.get_channel(channel_id=guild.voice_log_channel).send(embed=em)
            elif before.channel and after.channel:
                em = Embed(
                    title="Voice Log",
                    color=Color.gold(),
                    description=f"Member {member.mention} went from voice channel {before.channel.mention} to voice channel {after.channel.mention} :link:"
                )
                em.set_footer(text=f':clock1: {time} (Timezone: {timezone})')
                await member.guild.get_channel(channel_id=guild.voice_log_channel).send(embed=em)

    @Cog.listener()
    async def on_member_join(self, user):
        guild = get_server(user.guild.id)
        if guild.welcome_active:
            try:
                message = guild.welcome_message.replace("{name}", user.mention)
                await self.bot.get_channel(int(guild.welcome_channel)).send(message)
            except:
                print(f"{colors.RED}ERROR: CHANNEL NOT FOUND ON: {colors.YELLOW}{guild.id}{colors.END}")
        if guild.welcome_active_private:
            try:
                message = guild.welcome_message_private.replace("{name}", user.mention)
                await user.send(message)
            except:
                print(f"{colors.RED}ERROR: COULD NOT SEND MESSAGE TO: {colors.YELLOW}{user.name}{colors.END}")
        if guild.default_role_active:
            try:
                role = user.guild.get_role(role_id=guild.default_role)
                await user.add_roles(role, reason="Default role assigned!", atomic=True)
            except Exception as e:
                print(f"{colors.RED}ERROR: COULD NOT ADD ROLE FOR: {colors.YELLOW}{user.name}{colors.END}\n{e}")

    @Cog.listener()
    async def on_command_error(self, ctx: commands.Context, exc: BaseException):
        if isinstance(exc, BotOnHalt):
            pass
        elif isinstance(exc, MissingPermissions):
            await ctx.send(
                embed=Embed(
                    title="Error",
                    description="You need administrator permissions on this guild to perform this action!",
                    color=Color.red()))
        elif isinstance(exc, PermissionLevel):
            await ctx.send(
                embed=Embed(
                    title="Error",
                    description="Your permission level is not high enough to perform this action!",
                    color=Color.red()))
        elif isinstance(exc, MissingRequiredArgument):
            await ctx.send(
                embed=Embed(
                    title="Error",
                    description=f"The command you entered apparently requires more/other arguments, try\n\n**{ctx.prefix}help {ctx.command}**\n\nfor more informations on the command!",
                    color=Color.red()))
        elif isinstance(exc, BadArgument):
            await ctx.send(
                embed=Embed(
                    title="Error",
                    description="Apparently there was a formatting error in your message, if you meant to send a string longer than one word, and had quotation marks within the message, try to put a \ infront of it, if that wasn't your error, contact the bot devs about this!",
                    color=Color.red()))
        elif isinstance(exc, CommandNotFound):
            await ctx.send(
                embed=Embed(
                    title="Error",
                    description=str(exc.args[0]),
                    color=Color.red()))
        else:
            raise exc

    @commands.command()
    @syntax("<module name>")
    @checks.permission_check(4)
    async def reload_module(self, ctx: CommandContext, module: str):
        module = module.lower()
        if module.endswith(".py"):
            module = module[:-3]
        if module + ".py" in os.listdir("modules"):
            msg = await ctx.send("Reloading module `" + module.title() + "`!")
            try:
                self.bot.unload_extension("modules." + module)
                self.bot.load_extension("modules." + module)
            except Exception as e:
                await msg.edit(content=f"Could not reload module `{module.title()}`!\n```\n{e}```")
                return
            await msg.edit(content=f"The module `{module.title()}` has been reloaded!")
        else:
            modules = ""
            for m in os.listdir("modules"):
                if m == "__pycache__":
                    continue
                modules += m[:-3] + "\n"
            await ctx.send(f"That module cannot be found! Available modules are:```\n{modules}```")

    @commands.command()
    @syntax("<python code>")
    @checks.permission_check(4)
    async def eval(self, ctx: CommandContext, *, code: str = None):
        if not code:
            return await ctx.send(embed=Embed(title="Error", color=Color.red(), description="gimme code"))
        await drutils.handle_eval(ctx.message, self.bot, code)

    @commands.command()
    @syntax("<shell code>")
    @checks.permission_check(4)
    async def shell(self, ctx: CommandContext, *, code: str = None):
        if not code:
            return await ctx.send(embed=Embed(title="Error", color=Color.red(), description="gimme code"))
        if str(code).startswith("\""):
            code = str(code)[1:]
        if str(code).endswith("\""):
            code = str(code)[:-1]
        output = subprocess.Popen(str(code), shell=True, stdout=subprocess.PIPE)
        returned = output.stdout
        await ctx.send(embed=Embed(
            title="Console output:",
            description=f"```\n{returned.read().decode()}\n```",
            color=Color.blurple()
        ))
        output.kill()

    @commands.command()
    @checks.permission_check(4)
    async def halt(self, ctx: CommandContext):
        if not common.config["halted"]:
            common.config["halted"] = True
            await self.bot.change_presence(status=Status.dnd)
            await ctx.send(embed=Embed(
                title="Successfully halted!",
                description=f"The bot has been halted on the machine: **{socket.gethostname()}**.\nTo resume normal function, use **{ctx.prefix}resume**",
                color=Color.blurple()))
        else:
            await ctx.send(embed=Embed(
                title="Error",
                description=f"Bot already halted!\nUse **{ctx.prefix}resume** to resume normal function!",
                color=Color.red()))

    @commands.command()
    @checks.permission_check(4)
    async def resume(self, ctx: CommandContext):
        if common.config["halted"]:
            common.config["halted"] = False
            await self.bot.change_presence(status=Status.online)
            await ctx.send(embed=Embed(
                title="Successfully resumed!",
                description=f"The bot has been resumed to normal functionality on the machine: **{socket.gethostname()}**.\nTo halt it again, use **{ctx.prefix}halt**",
                color=Color.blurple()))
        else:
            await ctx.send(embed=Embed(
                title="Error",
                description=f"Bot is not halted!\nUse **{ctx.prefix}halt** to halt it!",
                color=Color.red()))

    @commands.command(description="Restarts the bot.")
    @checks.permission_check(4)
    async def restart(self, ctx: CommandContext):
        exit(10)


def setup(bot: commands.Bot):
    bot.add_cog(SystemCog(bot))
