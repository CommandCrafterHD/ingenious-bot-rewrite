from discord import Embed, Colour
from discord.ext import commands
from discord.ext.commands import Context as CommandContext, Cog
from utility import syntax
import checks


class TextEditCog(Cog):
    def __init__(self, bot: commands.Bot):
        self.bot: commands.Bot = bot

    @commands.command(description="13371fy y0ur t3xt5!")
    @syntax('<text>')
    async def leetify(self, ctx: CommandContext, *, text: str):
        replacement = {
            "a": "4",
            "b": "8",
            "e": "3",
            "g": "6",
            "i": "1",
            "o": "0",
            "s": "5",
            "t": "7",
            "z": "2"
        }

        text = text.lower()

        for i in replacement.keys():
            text = text.replace(i, replacement[i])

        await ctx.send(embed=Embed(
            title="Leetified it for you!",
            description=f"```fix\nLeetified =\n{text}```",
            color=Colour.green()
        ))

    @syntax('<text>')
    @commands.command(description="UwUfy your texts!")
    async def uwufy(self, ctx: CommandContext, *, text: str):
        replacement = {
            "l": "w",
            "r": "w"
        }

        text = text.lower()

        for i in replacement.keys():
            text = text.replace(i, replacement[i])

        text += " OwO!"

        await ctx.send(embed=Embed(
            title="Uwufied it for you! OwO",
            description=f"```fix\nUwufied =\n{text}```",
            color=Colour.green()
        ))

    @syntax('<title> <text> <color>')
    @checks.permission_check(2)
    @commands.command(description="Lets you create simple embeds, for longer texts use quotation marks! Accepted colors for the color input are:\n"
                                  "**red\n"
                                  "green\n"
                                  "blue\n"
                                  "yellow\n"
                                  "blurple\n"
                                  "**\n\nOr any HEX color code!")
    async def create_embed(self, ctx: CommandContext, title: str, text: str, color: str = None):
        if color.lower() in ["red", "green", "blue", "yellow", "blurple"]:
            if color.lower() == "red":
                color = Colour.red()
            elif color.lower() == "green":
                color = Colour.green()
            elif color.lower() == "blue":
                color = Colour.blue()
            elif color.lower() == "yellow":
                color = Colour.gold()
            elif color.lower() == "blurple":
                color = Colour.blurple()
        else:
            if color.startswith("#"):
                color = color[1:]
            color = int(color, 16)

        em = Embed(
            title=title,
            description=text,
            color=color
        )

        em.set_author(name=f"Created by {ctx.author.name}", icon_url=ctx.author.avatar_url)

        await ctx.send(embed=em)


def setup(bot: commands.Bot):
    bot.add_cog(TextEditCog(bot))
