from discord.ext import commands
from discord.ext.commands import Context as CommandContext, Cog
from discord import Embed, Colour
from utility import syntax, get_todo, add_todo, remove_todo, check_todo


class TodoCog(Cog):
    def __init__(self, bot: commands.Bot):
        self.bot: commands.Bot = bot

    @commands.command(description="Lets you add/remove/check off items of your ToDo list. Just replace the things in the brackets **<>** below, if you want to check off or remove all items just use * instead of a number!")
    @syntax('', 'add "<your item>"', 'remove <number of your item>', 'check <number of your item>')
    async def todo(self, ctx: CommandContext, *args: str):
        if args:
            if args[0].lower() == "add":
                if add_todo(uid=ctx.author.id, text=args[1]):
                    await ctx.send(embed=Embed(
                        title="Todo added",
                        description=f"Added new ToDo entry:```\n{args[1]}\n```",
                        color=Colour.green()
                    ))
                else:
                    await ctx.send(embed=Embed(
                        title="Error",
                        description="Maximum amount of ToDo entries reached! Remove some before adding new ones!",
                        color=Colour.red()
                    ))
            if args[0].lower() == "remove":
                if args[1] == "*":
                    returned = remove_todo(uid=ctx.author.id, tdid="*")
                else:
                    returned = remove_todo(uid=ctx.author.id, tdid=int(args[1]))
                if returned is "IE":
                    await ctx.send(embed=Embed(
                        title="Error",
                        description="Could not remove todo item! Looks like you don't have that many items!",
                        color=Colour.red()
                    ))
                    return
                elif returned is None:
                    await ctx.send(embed=Embed(
                        title="Error",
                        description="Could not remove todo item! Maybe you haven't added any yet!",
                        color=Colour.red()
                    ))
                    return
                else:
                    if args[1] == "*":
                        await ctx.send(embed=Embed(
                            title="Todo cleared",
                            description="Removed all ToDo items!",
                            color=Colour.green()
                        ))
                    else:
                        await ctx.send(embed=Embed(
                            title="Todo removed",
                            description="Removed ToDo item!",
                            color=Colour.green()
                        ))
            if args[0].lower() == "check":
                if args[1] == "*":
                    returned = check_todo(uid=ctx.author.id, tdid="*")
                else:
                    returned = check_todo(uid=ctx.author.id, tdid=int(args[1]))
                if returned is "IE":
                    await ctx.send(embed=Embed(
                        title="Error",
                        description="Could not remove todo item! Looks like you don't have that many items!",
                        color=Colour.red()
                    ))
                    return
                elif returned is None:
                    await ctx.send(embed=Embed(
                        title="Error",
                        description="Could not check todo item! Maybe you haven't added any yet!",
                        color=Colour.red()
                    ))
                    return
                else:
                    if args[1] == "*":
                        await ctx.send(embed=Embed(
                            title="Todo checked off",
                            description="Checked off all ToDo items!",
                            color=Colour.green()
                        ))
                    else:
                        await ctx.send(embed=Embed(
                            title="Todo checked",
                            description="Checked ToDo item!",
                            color=Colour.green()
                        ))
        else:
            todo = get_todo(uid=ctx.author.id).dicts()
            txt = ''
            for i in range(0, len(todo)):
                txt += f'**{i+1}**: {todo[i]["text"]}' + (' (:white_check_mark:)\n' if todo[i]["checked"] else '\n')
            if todo:
                await ctx.send(embed=Embed(
                     title="ToDo",
                     description=txt,
                     color=Colour.blurple()
                ))
            else:
                await ctx.send(embed=Embed(
                    title="ToDo",
                    description="Oh No! You dont have any ToDo items yet! Go and add some!",
                    color=Colour.gold()
                ))


def setup(bot: commands.Bot):
    bot.add_cog(TodoCog(bot))
