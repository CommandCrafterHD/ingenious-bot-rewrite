from utility import syntax
from discord import Embed, Colour
import random, requests, asyncio, html
from discord.ext import commands
from discord.ext.commands import Context as CommandContext, Cog


def get_question(difficulty: str):
    link = f"https://opentdb.com/api.php?amount=1&difficulty={difficulty}&type=multiple"
    request = requests.get(link).json()["results"][0]
    answers = [
        (request["correct_answer"], True)
    ]
    for i in request["incorrect_answers"]:
        unescaped = html.unescape(i)
        answers.append((unescaped, False))
    answers_shuffled = answers[:]
    random.shuffle(answers_shuffled)
    question = html.unescape(request["question"])
    return {
        "category": request["category"],
        "difficulty": request["difficulty"],
        "question": question,
        "answers": answers_shuffled,
        "correct_answer": request["correct_answer"]
    }


class TriviaCog(Cog):
    def __init__(self, bot: commands.Bot):
        self.bot: commands.Bot = bot

    @commands.command()
    @syntax('', '<easy/medium/hard>')
    async def trivia(self, ctx: CommandContext, difficulty: str = "easy"):
        difficulty = difficulty.lower()
        if difficulty != "easy":
            if difficulty not in ["medium", "hard"]:
                await ctx.send(embed=Embed(
                    title="Error",
                    color=Colour.red(),
                    description="The difficulty can only be: **easy | medium | hard**! Nothing else!"
                ))
                return
        question = get_question(difficulty)
        colors = {
            "easy": Colour.green(),
            "medium": Colour.gold(),
            "hard": Colour.red()
        }
        color = colors[difficulty]

        em = Embed(title="Trivia", description="", color=color)
        em.set_author(name="API by opentdb.com", url="https://opentdb.com")
        em.add_field(name="Category", value=question["category"])
        em.add_field(name="Difficulty", value=question["difficulty"].title(), inline=True)
        em.add_field(name="Question", value=question["question"], inline=False)

        answers = ""
        letters = [(":regional_indicator_a:", "🇦"), (":regional_indicator_b:", "🇧"),
                   (":regional_indicator_c:", "🇨"), (":regional_indicator_d:", "🇩")]
        for i in question["answers"]:
            answers += letters[question["answers"].index(i)][0] + " - " + i[0] + "\n"
        em.add_field(name="Answers", value=answers)
        msg = await ctx.send(embed=em)
        for i in question["answers"]:
            await msg.add_reaction(letters[question["answers"].index(i)][1])

        def check(reaction, user):
            if reaction.emoji in ["🇦", "🇧", "🇨", "🇩"] and user.id == ctx.author.id:
                return reaction

        try:
            reaction = await self.bot.wait_for('reaction_add', timeout=30.0, check=check)
        except asyncio.TimeoutError:
            em = Embed(title="TIME's UP!", description="", color=Colour.red())
            em.set_author(name="API by opentdb.com", url="https://opentdb.com")
            em.add_field(name="Category", value=question["category"])
            em.add_field(name="Difficulty", value=question["difficulty"].title(), inline=True)
            em.add_field(name="Question", value=question["question"], inline=False)
            em.add_field(name="Correct Answer", value="?")
            await msg.edit(embed=em)
            await msg.clear_reactions()
        else:
            react_index = ["🇦", "🇧", "🇨", "🇩"].index(reaction[0].emoji)
            answer = question["answers"][react_index]
            if answer[1]:
                em = Embed(title="CORRECT!", description="", color=Colour.green())
                em.set_author(name="API by opentdb.com", url="https://opentdb.com")
                em.add_field(name="Category", value=question["category"])
                em.add_field(name="Difficulty", value=question["difficulty"].title(), inline=True)
                em.add_field(name="Question", value=question["question"], inline=False)
                em.add_field(name="Correct Answer",
                             value=["🇦", "🇧", "🇨", "🇩"][react_index] + " - " + question["correct_answer"])
                await msg.edit(embed=em)
                await msg.clear_reactions()
            else:
                em = Embed(title="INCORRECT!", description="", color=Colour.red())
                em.set_author(name="API by opentdb.com", url="https://opentdb.com")
                em.add_field(name="Category", value=question["category"])
                em.add_field(name="Difficulty", value=question["difficulty"].title(), inline=True)
                em.add_field(name="Question", value=question["question"], inline=False)
                em.add_field(name="Correct Answer",
                             value=["🇦", "🇧", "🇨", "🇩"][list(dict(question["answers"]).keys()).index(question["correct_answer"])] + " - " + question["correct_answer"])
                await msg.edit(embed=em)
                await msg.clear_reactions()


def setup(bot: commands.Bot):
    bot.add_cog(TriviaCog(bot))
