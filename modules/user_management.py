import checks
from files import common
from utility import get_user, syntax
from discord import Embed, Colour, Member
from discord.ext import commands
from discord.ext.commands import Context as CommandContext, Cog


class UserManagementCog(Cog):
    def __init__(self, bot: commands.Bot):
        self.bot: commands.Bot = bot

    @commands.command()
    async def me(self, ctx: CommandContext):
        em = Embed(
            color=ctx.author.color,
            title=" ",
            description=" "
        )
        em.set_author(name=f"{ctx.author.name}#{ctx.author.discriminator}", icon_url=ctx.author.avatar_url)
        em.add_field(name="ID", value=str(ctx.author.id))
        em.add_field(name="Permission level", value=get_user(gid=ctx.guild.id, uid=ctx.author.id).permission_level, inline=True)
        em.add_field(name="Nickname", value=str(ctx.author.nick))
        em.add_field(name="Discriminator", value=str(ctx.author.discriminator), inline=True)
        await ctx.channel.send(embed=em)

    @commands.command()
    @syntax('<User> <Permission Level>')
    @checks.permission_check(3)
    async def permission_set(self, ctx: CommandContext, user: Member, pl: int):
        try:
            db_user = get_user(uid=user.id, gid=ctx.guild.id)
            if pl > 4:
                await ctx.send(embed=Embed(
                    title="Error",
                    description="You can only go up to permission level 4!",
                    color=Colour.red()
                ))
                return
            if pl > db_user.permission_level and str(ctx.author.id) not in common.config["admins"]:
                await ctx.send(embed=Embed(
                    title="Error",
                    description="You can not set someones permission level higher than your own!!",
                    color=Colour.red()
                ))
                return
            db_user.permission_level = int(pl)
            db_user.save()
            await ctx.channel.send(embed=Embed(
                color=Colour.green(),
                title="Success",
                description=f"{user.mention}'s new permission level is now: **{str(db_user.permission_level)}**!"
            ))
        except:
            await ctx.channel.send(embed=Embed(
                color=Colour.red(),
                title="Error",
                description="Something went wrong, maybe you didn't enter a number?"
            ))

    @commands.command()
    async def permission_list(self, ctx: CommandContext):
        em = Embed(
            color=Colour.blurple(),
            title="Permission Levels",
            description="The roles and their corresponding permission levels:"
        )
        em.add_field(name="Everyone", value="0", inline=False)
        em.add_field(name="VIP's", value="1", inline=False)
        em.add_field(name="Moderators", value="2", inline=False)
        em.add_field(name="Admins", value="3", inline=False)
        em.add_field(name="Developers", value="4", inline=False)
        await ctx.channel.send(embed=em)


def setup(bot: commands.Bot):
    bot.add_cog(UserManagementCog(bot))
