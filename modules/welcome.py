from discord import TextChannel, Embed, Color, Role
from discord.ext import commands
from discord.ext.commands import Context as CommandContext, Cog
from utility import get_server, syntax
from typing import Union
import checks


class WelcomeCog(Cog):
    def __init__(self, bot: commands.Bot):
        self.bot: commands.Bot = bot

    @commands.command(description="Set the server-wide welcome message! Just mention a textchannel and append your message after it. To mention the user who joined, use **{name}**\n To disable, just use **welcome_message disable**")
    @syntax('<channel> <message>', 'disable')
    @checks.permission_check(3)
    async def welcome_message(self, ctx: CommandContext, chn: Union[TextChannel, str] = None, *, msg: str = None):
        if chn and msg:
            if isinstance(chn, TextChannel):
                guild = get_server(ctx.guild.id)
                guild.welcome_active = True
                guild.welcome_channel = chn.id
                guild.welcome_message = msg
                guild.save()
                await ctx.send(embed=Embed(
                    title="Success",
                    description=f"Set the new message to: ```\n{msg}```and the channel for the messages will be: <#{chn.id}>",
                    color=Color.green()
                ))
            else:
                await ctx.send(embed=Embed(
                    title="Error",
                    description="You need to specify a text-channel and a message to send to said channel! Use **{}help welcome_set_message** for more info!".format(ctx.prefix),
                    color=Color.red()
                ))
        else:
            if chn:
                if chn.lower() == "disable":
                    guild = get_server(ctx.guild.id)

                    if not guild.welcome_active:
                        await ctx.send(embed=Embed(
                            title="Error",
                            description="The server-wide welcome message is already disabled!",
                            color=Color.red()
                        ))
                        return

                    guild.welcome_active = False
                    guild.save()
                    await ctx.send(embed=Embed(
                        title="Success",
                        description="Disabled the server-wide welcome message!",
                        color=Color.green()
                    ))
            else:
                guild = get_server(ctx.guild.id)
                if guild.welcome_active:
                    await ctx.send(embed=Embed(
                        title="Server-wide welcome",
                        color=Color.blurple(),
                        description="The current channel for welcoming new members is: <#" + str(guild.welcome_channel) + ">\nThe current welcome message is: ```\n" + str(guild.welcome_message) + "```"
                    ))
                else:
                    await ctx.send(embed=Embed(
                        title="Server-wide welcome",
                        color=Color.gold(),
                        description=f"Server wide welcome is currently disabled! Use **{ctx.prefix}help welcome_message** to find out how to enable it!"
                    ))

    @commands.command(description="Set the private welcome message! Just send your message. To mention the user who joined, use **{name}**\n To disable, just use **private_welcome_message disable**")
    @syntax('<message>')
    @checks.permission_check(3)
    async def private_welcome_message(self, ctx: CommandContext, *, msg: str = None):
        if msg:
            if msg.lower() == "disable":
                guild = get_server(ctx.guild.id)

                if not guild.welcome_active_private:
                    await ctx.send(embed=Embed(
                        title="Error",
                        description="The private welcome message is already disabled!",
                        color=Color.red()
                    ))
                    return

                guild.welcome_active_private = False
                guild.save()
                await ctx.send(embed=Embed(
                    title="Success",
                    description="Disabled the private welcome message!",
                    color=Color.green()
                ))
            else:
                guild = get_server(ctx.guild.id)
                guild.welcome_active_private = True
                guild.welcome_message_private = msg
                guild.save()
                await ctx.send(embed=Embed(
                    title="Success",
                    description=f"Set the new private message to: ```\n{msg}```",
                    color=Color.green()
                ))
        else:
            guild = get_server(ctx.guild.id)
            if guild.welcome_active_private:
                await ctx.send(embed=Embed(
                    title="Private welcome",
                    color=Color.blurple(),
                    description="The current welcome message is: ```\n" + str(
                        guild.welcome_message_private) + "```"
                ))
            else:
                await ctx.send(embed=Embed(
                    title="Private welcome",
                    color=Color.gold(),
                    description=f"Private welcome is currently disabled! Use **{ctx.prefix}help private_welcome_message** to find out how to enable it!"
                ))

    @commands.command(description="Set the default role! Just mention the role new users should get!\n To disable, just use **default_role disable**")
    @syntax('<Role Mention>', 'disable')
    @checks.permission_check(3)
    async def default_role(self, ctx: CommandContext, *, role: Union[Role, str] = None):
        if role:
            if isinstance(role, Role):
                guild = get_server(ctx.guild.id)
                if guild.default_role_active:
                    guild.default_role = int(role.id)
                    guild.save()
                    await ctx.send(embed=Embed(
                        title="Default role",
                        color=Color.green(),
                        description=f"Default role is now: {role.mention}!"
                    ))
                else:
                    guild.default_role = int(role.id)
                    guild.default_role_active = True
                    guild.save()
                    await ctx.send(embed=Embed(
                        title="Default role",
                        color=Color.green(),
                        description=f"Default role is now: {role.mention}!"
                    ))
            elif isinstance(role, str):
                if role.lower() == "disable":
                    guild = get_server(ctx.guild.id)
                    if guild.default_role_active:
                        guild.default_role_active = False
                        guild.save()
                        await ctx.send(embed=Embed(
                            title="Default role",
                            color=Color.green(),
                            description="Default role assignment has been disabled!"
                        ))
                    else:
                        await ctx.send(embed=Embed(
                            title="Error",
                            color=Color.red(),
                            description="Default role assignment is already disabled!"
                        ))
                else:
                    await ctx.send(embed=Embed(
                        title="Error",
                        color=Color.red(),
                        description=f"You need to mention a role to give or use disable to disable it all together, for more info just use\n**{ctx.prefix}help default_role**"
                    ))
        else:
            guild = get_server(ctx.guild.id)
            if guild.default_role_active:
                await ctx.send(embed=Embed(
                    title="Default role",
                    color=Color.green(),
                    description="The default role to be assigned is " + ctx.guild.get_role(role_id=guild.default_role).mention
                ))
            else:
                await ctx.send(embed=Embed(
                    title="Default role",
                    color=Color.gold(),
                    description=f"The default role is currently not set! To enable it, use **{ctx.prefix}default_role <Role mention>**"
                ))


def setup(bot: commands.Bot):
    bot.add_cog(WelcomeCog(bot))
