from utility import syntax
from discord import Embed, Colour, Emoji
import random, asyncio
from discord.ext import commands
from discord.ext.commands import Context as CommandContext, Cog


def replace_with_emoji(number: int):
    if number == 1:
        return ':one:'
    elif number == 2:
        return ':two:'
    elif number == 3:
        return ':three:'
    elif number == 4:
        return ':four:'
    elif number == 5:
        return ':five:'
    elif number == 6:
        return ':six:'


allowed_emojis = [str(x) + '\N{combining enclosing keycap}' for x in range(1, 7)]
allowed_emojis.append('🔄')


class YahtzeeCog(Cog):
    def __init__(self, bot: commands.Bot):
        self.bot: commands.Bot = bot

    @commands.command()
    @syntax('', 'rules')
    async def yahtzee(self, ctx: CommandContext, sub_command: str = None):
        if sub_command:
            if sub_command.lower() == 'rules':
                await ctx.send(embed=Embed(
                    title="Yahtzee (42/18) rules",
                    color=Colour.blurple(),
                    description="The rules are simple. Every round you throw all dice that are not \"out\"\n"
                                "You have to put one die out every round, you can put out as many as you want, but at least one is required!\n"
                                "To make a round count you need a 4 and a 2. All the other dice you put out count as your points.\n"
                                "So the maximum amount of points you could get would be 18 (because 4 and 2 don't count and the rest can only be 6 at most)"
                ))
            else:
                print(hex(ord(sub_command)))
        else:
            em = Embed(title="Yahtzee",
                       description="You have 4 throws left!",
                       color=Colour.green())

            put_aside = []
            throws = 4
            dice = [random.randrange(1, 7) for x in range(1, 6)]
            taken_this_round = 0

            em.add_field(name="Your dice", value=" ".join([replace_with_emoji(number=x) for x in dice]))
            msg = await ctx.send(embed=em)

            while len(dice) >= 0:
                if len(dice) == 0:
                    if 4 in put_aside and 2 in put_aside:
                        points = -6
                        for p in put_aside:
                            points += p
                        em = Embed(
                            title="Yahtzee",
                            description=f"Done! You got {points} points!",
                            color=Colour.green()
                        )
                    else:
                        em = Embed(
                            title="Yahtzee",
                            description="Done! Sadly you don't have a 2 and a 4, why don't you try again?",
                            color=Colour.red()
                        )
                    await msg.edit(embed=em)
                    await msg.clear_reactions()
                    break

                await msg.clear_reactions()

                if throws > 0:
                    desc = f"You have {throws} throws left!"
                else:
                    desc = "Last throw!"

                em = Embed(title="Yahtzee",
                           description=desc,
                           color=Colour.green())
                if len(put_aside) > 0:
                    em.add_field(name="Put aside", value=" ".join([replace_with_emoji(number=x) for x in put_aside]), inline=False)
                em.add_field(name="Your dice", value=" ".join([replace_with_emoji(number=x) for x in dice]), inline=False)
                await msg.edit(embed=em)

                for i in range(1, len(dice) + 1):
                    await msg.add_reaction(str(i) + '\N{combining enclosing keycap}')
                if throws > 0:
                    await msg.add_reaction('🔄')

                def check(reaction, user):
                    if reaction.emoji in allowed_emojis and user.id == ctx.author.id:
                        return reaction

                try:
                    reaction = await self.bot.wait_for('reaction_add', timeout=30.0, check=check)
                except asyncio.TimeoutError:
                    await msg.edit(embed=Embed(
                        title="Time's up!",
                        description="You took too long to pick a die to take out!",
                        color=Colour.red()
                    ))
                    await msg.clear_reactions()
                    break

                if str(reaction[0].emoji) == '🔄':
                    if throws > 0:
                        if taken_this_round == 0:
                            pass
                        else:
                            dice = [random.randrange(1, 7) for x in range(1, len(dice)+1)]
                            throws -= 1
                            taken_this_round = 0
                            continue
                else:
                    picked = int(str(reaction[0].emoji)[0])-1
                    if picked <= len(dice):
                        put_aside.append(dice[picked])
                        dice.remove(dice[picked])
                        taken_this_round += 1
                    else:
                        pass


def setup(bot: commands.Bot):
    bot.add_cog(YahtzeeCog(bot))
