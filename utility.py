# DB Stuffs (Here's where all the stuff like prefixes gets loaded from)
from peewee import SqliteDatabase, Model, BigIntegerField, CharField, OperationalError, BooleanField, TextField, IntegerField, BigAutoField
import yaml

db = SqliteDatabase('files/servers.db')

permission_db = SqliteDatabase('files/users.db')

todo_db = SqliteDatabase('files/todo.db')

try:
    db.connect()
    db.close()
except OperationalError:
    db = SqliteDatabase('../files/servers.db')

try:
    permission_db.connect()
    permission_db.close()
except OperationalError:
    permission_db = SqliteDatabase('../files/users.db')

try:
    todo_db.connect()
    todo_db.close()
except OperationalError:
    todo_db = SqliteDatabase('../files/todo.db')


class Server(Model):
    # The servers ID
    id = BigIntegerField()

    # --== General Config ==--
    prefix = CharField()

    # --== Welcome module ==--
    welcome_active = BooleanField(default=False)
    welcome_channel = BigIntegerField(default=10)
    welcome_message = TextField(default="Welcome!")

    welcome_active_private = BooleanField(default=False)
    welcome_message_private = TextField(default="Welcome!")

    default_role_active = BooleanField(default=False)
    default_role = BigIntegerField(default=10)

    # --== Logging module ==--
    voice_log_active = BooleanField(default=False)
    voice_log_channel = BigIntegerField(default=10)

    class Meta:
        database = db


db.connect()
db.create_tables([Server])
db.close()


def add_server(gid):
    server = Server.get_or_none(Server.id == int(gid))
    if server:
        raise FileExistsError("Server already exists with ID: " + str(gid) + "! Remove it from database before trying to add it again!")
    else:
        prefix = yaml.load(open("files/config.yaml", "r"), Loader=yaml.SafeLoader)["prefix"]
        Server.create(id=gid, prefix=prefix)
    return server


def get_server(gid, add_if_none: bool = True):
    server = Server.get_or_none(Server.id == int(gid))
    if server:
        return server
    else:
        if add_if_none:
            return add_server(gid)
        else:
            return "Server not found!"


class User(Model):
    gid = BigIntegerField()
    uid = BigIntegerField()

    permission_level = IntegerField()

    class Meta:
        database = permission_db


permission_db.connect()
permission_db.create_tables([User])
permission_db.close()


def add_user(uid: int, gid: int):
    user = User.get_or_none(User.uid == int(uid), User.gid == int(gid))
    if user:
        raise FileExistsError(
            "User already exists with ID: " + str(uid) + " and Guild-ID " + str(gid) + "! Remove it from database before trying to add it again!")
    else:
        user = User.create(uid=uid, gid=gid, permission_level=1)
    return user


def get_user(uid: int, gid: int):
    user = User.get_or_none(User.uid == int(uid), User.gid == int(gid))
    if user:
        return user
    else:
        return add_user(uid, gid)


class Todo(Model):
    tdid = BigAutoField()
    uid = BigIntegerField()
    text = TextField()
    checked = BooleanField(default=False)

    class Meta:
        database = todo_db


todo_db.connect()
todo_db.create_tables([Todo])
todo_db.close()


def get_todo(uid):
    todo = Todo.select().where(Todo.uid == int(uid)).order_by(Todo.tdid)
    return todo


def add_todo(uid, text):
    if len(get_todo(uid)) <= 10:
        return Todo.create(uid=uid, text=str(text))
    else:
        return None


def remove_todo(uid, tdid):
    if len(get_todo(uid)) == 0:
        return None
    else:
        if tdid == "*":
            todo = Todo.delete().where(Todo.uid == uid)
            return todo.execute()
        try:
            del_id = get_todo(uid)[tdid-1].tdid
            todo = Todo.delete().where(Todo.uid == uid, Todo.tdid == del_id)
            return todo.execute()
        except IndexError:
            return "IE"


def check_todo(uid, tdid):
    if len(get_todo(uid)) == 0:
        return None
    else:
        if tdid == "*":
            todos = get_todo(uid)
            for i in todos:
                i.checked = True
                i.save()
            return "Done"
        try:
            todo = get_todo(uid)[int(tdid)-1]
            todo.checked = True
            todo.save()
            return "Done"
        except IndexError:
            return "IE"


# syntax's for commands
def syntax(*txt):
    def decorator(func):
        func.syntax = txt
        return func
    return decorator
