from aiohttp import web
from aiohttp_session import get_session, setup
from aiohttp_session.cookie_storage import EncryptedCookieStorage
from cryptography import fernet
import base64
import time
import aiohttp_jinja2
import jinja2
import os
import yaml
import requests
from utility import get_server

routes = web.RouteTableDef()
routes.static('/static', os.path.dirname(os.path.abspath(__file__)) + '/static')

config = yaml.load(open("../files/config.yaml", "r"), Loader=yaml.SafeLoader)

SESSION_TYPE = 'memcache'
API_BASE_URL = 'https://discordapp.com/api/v6'
BASE_URL = ""


def get_discord_user(token):
    if token is None:
        return None
    resp = requests.get(API_BASE_URL + '/users/@me',
                        headers={
                            'Authorization': 'Bearer ' + token
                        })
    if 400 <= resp.status_code < 500:
        return None
    return resp.json()


def get_discord_id(token):
    user = get_discord_user(token)
    if user is not None:
        return user['id']
    return None


def get_avatar_url(user_dict):
    if user_dict is None:
        return 'none.png'
    if user_dict['avatar'] is None:
        defaults = ["https://discordapp.com/assets/6debd47ed13483642cf09e832ed0bc1b.png",
                    "https://discordapp.com/assets/322c936a8c8be1b803cd94861bdfa868.png",
                    "https://discordapp.com/assets/dd4dbc0016779df1378e7812eabaa04d.png",
                    "https://discordapp.com/assets/0e291f67c9274a1abdddeb3fd919cbaa.png",
                    "https://discordapp.com/assets/1cbd08c76f8af6dddce02c5138971129.png"]
        to_return = int(user_dict['discriminator']) % 5
        return defaults[to_return]
    return "https://cdn.discordapp.com/avatars/" + user_dict['id'] + '/' + user_dict['avatar'] + (
        '.gif' if user_dict['avatar'][0:2] == 'a_' else '.jpg')


async def is_logged_in(request):
    session = await get_session(request)
    if 'token' in session and get_discord_user(session['token']) is not None:
        return True
    else:
        return False


async def get_current_discord_user(request):
    session = await get_session(request)
    return get_discord_user(session['token'])


def get_bot_guild(gid):
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bot ' + config["token"]
    }
    r = requests.get(f'https://discordapp.com/api/v6/guilds/{str(gid)}', headers=headers)
    if r.json() is not None:
        return r.json()
    else:
        return None


def get_bot_channels(gid):
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bot ' + config["token"]
    }
    r = requests.get(f'https://discordapp.com/api/v6/guilds/{str(gid)}/channels', headers=headers)
    if r.json() is not None:
        return r.json()
    else:
        return None


async def get_guilds(token, refresh, request):
    session = await get_session(request)
    try:
        if 'guilds' in session and not refresh:
            return session['guilds']
            pass
    except KeyError:
        pass
    if token is None:
        return None
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + token
    }

    r = requests.get('https://discordapp.com/api/users/@me/guilds', headers=headers)
    r = r.json()
    guilds = []
    for i in r:
        if not i["owner"]:
            continue
        else:
            guilds.append(i)

    bot_guilds = []

    for g in guilds:
        bot_guild = get_bot_guild(g["id"])
        try:
            id = bot_guild["id"]
            if id:
                guilds.remove(g)
                bot_guilds.append(bot_guild)
        except KeyError:
            continue

    session['guilds'] = bot_guilds
    return bot_guilds


def update_guild_config(guid: int, new_config: dict):
    guild = get_server(guid, add_if_none=True)

    guild.prefix = new_config["prefix"][0]
    guild.save()


def exchange_code(code):
    data = {
        'client_id': config['client_id'],
        'client_secret': config['client_secret'],
        'grant_type': 'authorization_code',
        'code': code,
        'redirect_uri': 'http://localhost:5000/auth',
        'scope': 'identify email connections'
    }
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    r = requests.post('https://discordapp.com/api/v6/oauth2/token', data=data, headers=headers)
    try:
        token = r.json()['access_token']
    except:
        token = None
    return token


@routes.get('/auth')
async def auth(request):
    session = await get_session(request)
    code = request.rel_url.query['code']
    token = exchange_code(code)
    """error = response['error']
    if error == "access_denied":
        return web.HTTPFound('/')"""
    session['token'] = token
    return web.HTTPFound('/dashboard')


@routes.get('/servers')
async def servers(request):
    if await is_logged_in(request):
        session = await get_session(request)
        guild_list = await get_guilds(session['token'], refresh=True, request=request)
        user_dict = await get_current_discord_user(request)
        return aiohttp_jinja2.render_template('servers.jinja2', request, {"values": {
            'user': user_dict,
            'user_avatar': get_avatar_url(user_dict)
        }, "guilds": guild_list})
    else:
        return web.HTTPFound('/')


@routes.get('/server/{guid}')
@routes.post('/server/{guid}')
async def server(request):
    if await is_logged_in(request):
        session = await get_session(request)
        guid = request.match_info['guid']
        if guid:
            gconfig = get_server(guid)
            if gconfig == "Server not found!":
                await four_o_four(request)
            else:
                user_dict = await get_current_discord_user(request)
                guild_found = None
                for guild in await get_guilds(session['token'], refresh=False, request=request):
                    if guild["id"] == str(guid):
                        guild_found = guild
                        break
                channels = get_bot_channels(guild_found["id"])
                success = False
                try:
                    request_data = await request.post()
                except:
                    request_data = None
                if request_data:
                    update_guild_config(int(guid), request_data)
                    success = True
                    gconfig = get_server(guid)
                return aiohttp_jinja2.render_template('server.jinja2', request, {"values": {
                    'user': user_dict,
                    'user_avatar': get_avatar_url(user_dict),
                    'guild': guild_found,
                    'channels': channels,
                    'successfull_update': success
                }, "config":gconfig})
        else:
            await four_o_four(request)
    else:
        return web.HTTPFound('/')


@routes.get('/dashboard')
async def dashboard(request):
    if await is_logged_in(request):
        user_dict = await get_current_discord_user(request)
        return aiohttp_jinja2.render_template("dashboard.jinja2", request, {"values": {
            'user': user_dict,
            'user_avatar': get_avatar_url(user_dict)
        }})
    else:
        return web.HTTPFound('/')


@routes.get('/')
@aiohttp_jinja2.template('index.jinja2')
async def index(request):
    global BASE_URL
    if BASE_URL == "":
        print("BASE_URL is now: " + str(request.url))
        BASE_URL = request.url
    return aiohttp_jinja2.render_template("index.jinja2", request, {})


@routes.get('/imprint')
async def imprint(request):
    if await is_logged_in(request):
        user_dict = await get_current_discord_user(request)
        return aiohttp_jinja2.render_template('imprint.jinja2', request, {"values": {
            'user': user_dict,
            'user_avatar': get_avatar_url(user_dict)
        }})
    else:
        return aiohttp_jinja2.render_template('imprint.jinja2', request, {"values": {
            'user': None,
            'user_avatar': None
        }})


@routes.get('/privacy')
async def privacy_policy(request):
    if await is_logged_in(request):
        user_dict = await get_current_discord_user(request)
        return aiohttp_jinja2.render_template('privacy_policy.jinja2', request, {"values": {
            'user': user_dict,
            'user_avatar': get_avatar_url(user_dict)
        }})
    else:
        return aiohttp_jinja2.render_template('privacy_policy.jinja2', request, {"values": {
            'user': None,
            'user_avatar': None
        }})


@routes.get('/disclaimer')
async def disclaimer(request):
    if await is_logged_in(request):
        user_dict = await get_current_discord_user(request)
        return aiohttp_jinja2.render_template('disclaimer.jinja2', request, {"values": {
            'user': user_dict,
            'user_avatar': get_avatar_url(user_dict)
        }})
    else:
        return aiohttp_jinja2.render_template('disclaimer.jinja2', request, {"values": {
            'user': None,
            'user_avatar': None
        }})


@routes.get('/logout')
async def logout(request):
    session = await get_session(request)
    session['token'] = None
    return web.HTTPFound('/')


# ERROR-HANDLER
async def four_o_four(request):
    if is_logged_in(request):
        user_dict = await get_current_discord_user(request)
        return aiohttp_jinja2.render_template('404.jinja2', request, {"values": {
            'user': user_dict,
            'user_avatar': get_avatar_url(user_dict)
        }})
    else:
        return web.HTTPFound('/')


app = web.Application()
fernet_key = fernet.Fernet.generate_key()
secret_key = base64.urlsafe_b64decode(fernet_key)
setup(app, EncryptedCookieStorage(secret_key))
app.add_routes(routes)
aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader('templates'))

# Start server
web.run_app(app, port=5000)
